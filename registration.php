<?php
require_once("dbConnect.php");

if (isset($_POST['email']) && isset($_POST['nickname']) && isset($_POST['password']) && isset($_POST['psw_repeat']))
{
    $email = $_POST['email'];
    $nickname = $_POST['nickname'];
    $password = $_POST['password'];
    $psw_repeat = $_POST['psw_repeat'];

    if (!empty($email) && !empty($nickname) && !empty($password) && !empty($psw_repeat))
    {
        if ($password === $psw_repeat)
        {
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            $dbh = dbConnect("blog_eva", "root", "");
            $insert_user = "INSERT INTO user (nickname, email, passwordHash) 
                            VALUES  (:nickname, :email, :password)";
            $stmt = $dbh->prepare($insert_user);
            $stmt->execute([
                ":nickname" => $nickname,
                ":email" => $email,
                ":password" => $password_hash,
            ]);
        }
    } 
}
?>
<form action="registration.php" method="POST">
    <label for="email">Enter your email:</label>
    <input id="email" type="email" name="email">
    <label for="nickname">Pseudo</label>
    <input id="nickname" name="nickname" type="text">
    <label for="password">Mot de passe</label>
    <input id="password" name="password" type="password">
    <label for="psw_repeat">Repeat password</label>
    <input id="psw_repeat" name="psw_repeat" type="password">
    <input type="submit" value="Connexion">
</form>

</html>